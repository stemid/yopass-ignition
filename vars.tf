variable "username" {
  type = string
}

variable "uid" {
  type = number
}

variable "gid" {
  type = number
}

variable "redis_host" {
  type = string
}
