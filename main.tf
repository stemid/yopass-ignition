data "ignition_file" "yopass_container_unit" {
  path = "/home/${var.username}/.config/containers/systemd/yopass.container"
  content {
    content = templatefile("${path.module}/templates/yopass.container", {
      redis_host = var.redis_host
    })
  }
  uid = var.uid
  gid = var.gid
}

data "ignition_config" "config" {
  files = [
    data.ignition_file.yopass_container_unit.rendered
  ]
}
